#!/bin/bash
#
# Deploy files using SCP http://manpages.ubuntu.com/manpages/trusty/en/man1/scp.1.html
# Bitbucket Pipelines
#
# Required globals:
#   USER
#   SERVER
#   REMOTE_PATH
#   LOCAL_PATH
#
# Optional globals:
#   SSH_KEY
#   EXTRA_ARGS
#   DEBUG

# Begin Standard 'imports'
set -e
set -o pipefail

grey="\\e[37m"
blue="\\e[36m"
red="\\e[31m"
green="\\e[32m"
reset="\\e[0m"

# Logging, loosely based on http://www.ludovicocaldara.net/dba/bash-tips-4-use-logging-levels/
info() { echo -e "${blue}INFO: $*${reset}"; }
error() { echo -e "${red}ERROR: $*${reset}"; }
debug() { if [[ "${DEBUG}" == "true" ]]; then echo -e "${grey}DEBUG: $*${reset}"; fi }
success() { echo -e "${green}✔ $*${reset}"; }
fail() { echo -e "${red}✖ $*${reset}"; }


## Enable debug mode.
SCP_DEBUG_ARGS=
enable_debug() {
  if [[ "${DEBUG}" == "true" ]]; then
    info "Enabling debug mode."
    set -x
    SCP_DEBUG_ARGS="-v"
  fi
}

# Execute a command, saving its exit status code.
# Globals set:
#   status: Exit status of the command that was executed.
#
run() {
  echo "$@"
  set +e
  "$@" 2>&1
  status=$?
  set -e
}

# End standard 'imports'


validate() {
  # mandatory parameters
  : USER=${USER:?'SSH_USER variable missing.'}
  : SERVER=${SERVER:?'SSH_SERVER variable missing.'}
  : REMOTE_PATH=${REMOTE_PATH:?'SSH_REMOTE_PATH variable missing.'}
  : LOCAL_PATH=${LOCAL_PATH:?'LOCAL_PATH variable missing.'}
}

setup_ssh_dir() {
  INJECTED_SSH_CONFIG_DIR="/opt/atlassian/pipelines/agent/ssh"
  # The default ssh key with open perms readable by alt uids
  IDENTITY_FILE="${INJECTED_SSH_CONFIG_DIR}/id_rsa_tmp"
  # The default known_hosts file
  KNOWN_HOSTS_FILE="${INJECTED_SSH_CONFIG_DIR}/known_hosts"

  mkdir -p ~/.ssh || debug "adding ssh keys to existing ~/.ssh"
  touch ~/.ssh/authorized_keys

  # If given, use SSH_KEY, otherwise check if the default is configured and use it
  if [ "${SSH_KEY}" != "" ]; then
     debug "Using passed SSH_KEY"
     (umask  077 ; echo ${SSH_KEY} | base64 -d > ~/.ssh/pipelines_id)
  elif [ ! -f ${IDENTITY_FILE} ]; then
     error "No default SSH key configured in Pipelines."
     exit 1
  else
     debug "Using default ssh key"
     cp ${IDENTITY_FILE} ~/.ssh/pipelines_id
  fi

  if [ ! -f ${KNOWN_HOSTS_FILE} ]; then
      error "No SSH known_hosts configured in Pipelines."
      exit 2
  fi

  cat ${KNOWN_HOSTS_FILE} >> ~/.ssh/known_hosts
  if [ -f ~/.ssh/config ]; then
      debug "Appending to existing ~/.ssh/config file"
  fi
  echo "IdentityFile ~/.ssh/pipelines_id" >> ~/.ssh/config
  chmod -R go-rwx ~/.ssh/
}

run_pipe() {
  info "Starting SCP deployment to ${SERVER}:${REMOTE_PATH}..."
  run scp -rp -i ${IDENTITY_FILE} ${SCP_DEBUG_ARGS} ${EXTRA_ARGS} ${LOCAL_PATH} ${USER}@${SERVER}:${REMOTE_PATH}

  if [[ "${status}" == "0" ]]; then
    success "Deployment finished."
  else
    fail "Deployment failed."
  fi

  exit $status
}

validate
setup_ssh_dir
run_pipe
